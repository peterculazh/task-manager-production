// CRUD create read update delete

// const mongodb = require('mongodb');
// const MongoClient = mongodb.MongoClient;
// const ObjectID = mongodb.ObjectID;

const {
    MongoClient,
    ObjectID
} = require('mongodb');

const connectionURL = 'mongodb://127.0.0.1:27017';
const databaseName = 'task-manager';

MongoClient.connect(connectionURL, {
    useNewUrlParser: true
}, (error, client) => {
    if (error) {
        console.log(error);
    }

    const db = client.db(databaseName);

    // Insert one document in collections "Create"

    // db.collection('users').insertOne({
    //     _id: id,
    //     name: "Vikram",
    //     age: 26
    // }, (error, result) => {
    //     if (error) {
    //         return console.log('Unable to insert user');
    //     }
    //     console.log(result.ops);
    // });

    // Insert many rows in collections

    // db.collection('users').insertMany([{
    //     name: "Jen",
    //     age: 28
    // }, {
    //     name: 'Gunther',
    //     age: 27
    // }], (error, result) => {
    //     if(error){
    //         return console.log('Unable to insert documents: ', error);
    //     }
    //     console.log(result.ops);
    // });

    // db.collection('tasks').insertMany([{
    //         description: "First task",
    //         completed: false
    //     },
    //     {
    //         description: "Hard task",
    //         completed: false
    //     },
    //     {
    //         description: "Easy task",
    //         completed: true
    //     }
    // ], (error, result) => {
    //     if (error) {
    //         return console.log("Ooops. Something gone wrong: " + error);
    //     }
    //     console.log(result);
    // });

    // db.collection("users").findOne({
    //     _id: new ObjectID("5d85ed49ea9ad73498ad6798")
    // }, (error, user) => {
    //     if (error) {
    //         return console.log("Error: " + error);
    //     } else if (user === null) {
    //         return console.log("User no found");
    //     }
    //     console.log(user);
    // });

    // db.collection("users").find({
    //     age: 27
    // }).toArray((error, users) => {
    //     console.log(users);
    // });

    // db.collection("users").find({
    //     age: 27
    // }).count((error, users) => {
    //     console.log(users);
    // });

    // db.collection("tasks").findOne({
    //     _id: new ObjectID("5d85ee97b1885840a8462a01")
    // }, (error, task) => {
    //     console.log(task);
    // });

    // db.collection('tasks').find({
    //     completed: false
    // }).toArray((error, tasks) => {
    //     console.log(tasks);
    // });


    // Updating


    // db.collection('users').updateOne({
    //     _id: ObjectID("5d85ed49ea9ad73498ad6798")
    // }, {
    //     $inc: {
    //         age: 1
    //     }
    // }).then(
    //     result => {
    //         console.log(result);
    //     },
    //     error => {
    //         console.log(error);
    //     }
    // );

    // db.collection('tasks').updateMany({
    //     completed: true
    // }, {
    //     $set: {
    //         completed: false
    //     }
    // }).then(
    //     result => {
    //         console.log(result);
    //     },
    //     error => {
    //         console.log(error);
    //     }
    // );

    // Removing or DELETING

    // db.collection('users').deleteMany({
    //     age: 27
    // }).then(result => {
    //     console.log(result);
    // }, error => {
    //     console.log(error);
    // });

    // db.collection('tasks').deleteOne({
    //     description :"Hard task"
    // }).then(result => {
    //     console.log(result);
    // }, error => {
    //     console.log(error);
    // });
});