const sgMail = require("@sendgrid/mail");

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

function sendWelcomeEmail(email, name) {
    sgMail.send({
        to: email,
        from: "peterculazh@gmail.com",
        subject: `Hello. Welcome to Task site ${name}`,
        text: `Hello. We are welcome your on our site. Hope you will enjoy it. Good luck ${name}`,
        html: `<h1>Hello ${name}</h1>`
    });
}

function sendRemoveEmail(email,name) {
    sgMail.send({
        to: email,
        from: "peterculazh@gmail.com",
        subject: "Deleting account",
        text: "Hello",
        html: `<h1>Hello.</h1><br>${name}, we are sad to hear your deleting account. Write us what is wrong!`
    })
}

module.exports = {
    sendWelcomeEmail,
    sendRemoveEmail
};