const mongoose = require('mongoose');

mongoose.connect(process.env.MONGODB_URL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: true
});

// Creating a model of collection

// const User = mongoose.model('User', {
//     name: {
//         type: String,
//         required: true,
//         trim: true
//     },
//     email: {
//         type: String,
//         required: true,
//         trim: true,
//         lowercase: true,
//         validate(value) {
//             if (!validator.isEmail(value)) {
//                 throw new Error("Email is invalid");
//             }
//         }
//     },
//     age: {
//         type: Number,
//         default: 0,
//         validate(value) {
//             if (value < 0) {
//                 throw new Error("Age must be a positive number");
//             }
//         }
//     },
//     password: {
//         type: String,
//         required: true,
//         minlength: 7,
//         trim: true,
//         validate(value) {
//             if (value.toLowerCase().includes("password")) {
//                 throw new Error("This password is unsafe");
//             }
//         }
//     }
// });

// Creating a instance

// const me = new User({
//     name: " Ketty  ",
//     email: "KAY@gmail.com",
//     password: "password"
// });

// Saving a created about instance in DB

// me.save().then(
//     result => {
//         console.log(result);
//     },
//     error => {
//         console.log("Error!", error);
//     }
// );

// const Task = mongoose.model("Task", {
//     description: {
//         type: String,
//         required: true,
//         trim: true,
//     },
//     completed: {
//         type: Boolean,
//         default: false
//     }
// });

// const newTask = new Task({
//     description: "First task"
// });

// newTask.save().then(result => {
//     console.log(result);
// }, error => {
//     console.log(error);
// });

